from nltk import FreqDist
from nltk.tokenize import word_tokenize as word_tokenize_eng
from hazm import Normalizer as FaNormalizer
from hazm import word_tokenize as word_tokenize_fa

import sys


help_text = """Usage
key_words_finder.py textfile.txt  stopwords.txt """

if sys.argv[0] in ['-h', '--help'] or len(sys.argv) <= 2:
    print(help_text)
    exit()



# get args from cli
text_file_name = sys.argv[1]
stop_words_file_name = sys.argv[2]


# read from files
with open(text_file_name) as f:
    text = f.read()

with open(stop_words_file_name) as f:
    stop_words = f.read()

# normalize ant tokenize

if 'fa' in stop_words_file_name:
    normalizer = FaNormalizer()
    text = normalizer.normalize(text)
    text = word_tokenize_fa(text)
else:
    text = word_tokenize_eng(text)
stop_words = stop_words.split("\n")


# remove stop words from words
#clean_words = []
#for word in text:
#    if word not in stop_words:
#        clean_words.append(word)

clean_word = [word for word in text if word not in stop_words]


# get 15 most repeated words in text
clean_words = FreqDist(clean_words)
for common_word in clean_words.most_common(15):
    print(common_word) 
